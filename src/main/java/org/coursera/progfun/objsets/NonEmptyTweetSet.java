package org.coursera.progfun.objsets;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
class NonEmptyTweetSet extends TweetSet {

    private final Tweet elem;
    private final TweetSet left;
    private final TweetSet right;

    public NonEmptyTweetSet(Tweet elem, TweetSet left, TweetSet right) {
        this.elem = elem;
        this.left = left;
        this.right = right;
    }

    @Override
    protected TweetSet filterAcc(Function<Tweet, Boolean> p, TweetSet acc) {
        return right.filterAcc(p,
            left.filterAcc(p,
                p.apply(elem) ? acc.incl(elem) : acc
                ));
    }

    @Override
    public TweetSet union(TweetSet that) {
        final TreeSet<Tweet> t = new TreeSet<>();
        t.addAll(asSet());
        t.addAll(that.asSet());

        return inclAcc(tail(t), new EmptyTweetSet().incl(t.first()));
    }

    private TreeSet<Tweet> tail(TreeSet<Tweet> t) {
        return new TreeSet<>(t.tailSet(t.first(), false));
    }

    private TweetSet inclAcc(TreeSet<Tweet> that, TweetSet acc) {
        return that.isEmpty() ? acc
                : inclAcc(tail(that), acc.incl(that.first()));
    }

    @Override
    public Set<Tweet> asSet() {
        Set<Tweet> result = new HashSet<>(left.asSet());
        result.addAll(right.asSet());
        result.add(elem);
        return result;
    }

    @Override
    public Tweet mostRetweeted() {
        return asSet().stream().sorted((t1, t2) -> -Integer.compare(t1.retweet, t2.retweet)).findFirst().get();
    }

    @Override
    public TweetList descendingByRetweet() {
        final Tweet mostRetweeted = mostRetweeted();
        return new NonEmptyTweetList(mostRetweeted, remove(mostRetweeted).descendingByRetweet());
    }

    @Override
    public TweetSet incl(Tweet x) {
        return  x.before(elem) ? new NonEmptyTweetSet(elem, left.incl(x), right)
                : elem.before(x) ? new NonEmptyTweetSet(elem, left, right.incl(x))
                : this;
    }

    @Override
    public TweetSet remove(Tweet x) {
        return  x.before(elem) ? new NonEmptyTweetSet(elem, left.remove(x), right)
                : elem.before(x) ? new NonEmptyTweetSet(elem, left, right.remove(x))
                : left.union(right);
    }

    @Override
    public Boolean contains(Tweet x) {
        return  x.before(elem) ? left.contains(x)
                : elem.before(x) ? right.contains(x)
                : true;
    }

    @Override
    public void foreach(Consumer<Tweet> f) {
        f.accept(elem);
        left.foreach(f);
        right.foreach(f);
    }
}
