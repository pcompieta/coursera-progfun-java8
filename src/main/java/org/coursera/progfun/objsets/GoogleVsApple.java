package org.coursera.progfun.objsets;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class GoogleVsApple {

    private static final List<String> google = Arrays.asList("android", "Android", "galaxy", "Galaxy", "nexus", "Nexus");
    private static final List<String> apple = Arrays.asList("ios", "iOS", "iphone", "iPhone", "ipad", "iPad");

    public static void main(String[] args) throws IOException {
        TweetSet allTweets = tweetReader();
        TweetSet googleTweets = allTweets.filter(t -> google.stream().anyMatch(s -> t.getText().contains(s)));
        TweetSet appleTweets = allTweets.filter(t -> apple.stream().anyMatch(s -> t.getText().contains(s)));
        googleTweets.union(appleTweets).descendingByRetweet().foreach(System.out::println);
    }

    private static TweetSet tweetReader() throws IOException {
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("org/coursera/progfun/objsets/TweetData.json")) {
            return new TweetReader(is).allTweets();
        }
    }
}
