package org.coursera.progfun.objsets;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class NonEmptyTweetList implements TweetList {

    private final Tweet head;
    private final TweetList tail;

    public NonEmptyTweetList(Tweet head, TweetList tail) {
        this.head = head;
        this.tail = tail;
    }

    @Override
    public Tweet head() {
        return head;
    }

    @Override
    public TweetList tail() {
        return tail;
    }

    @Override
    public Boolean isEmpty() {
        return false;
    }
}
