package org.coursera.progfun.objsets;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public abstract class TweetSet {

    public int size() {
        return asSet().size();
    }

    @Override
    public String toString() {
        return asSet().toString();
    }

    /**
     * This method takes a predicate and returns a subset of all the elements
     * in the original set for which the predicate is true.
     */
    public TweetSet filter(Function<Tweet, Boolean> function) {
        return filterAcc(function, new EmptyTweetSet());
    }

    /**
     * This is a helper method for `filter` that propagetes the accumulated tweets.
     */
    protected abstract TweetSet filterAcc(Function<Tweet, Boolean> function, TweetSet tweetSet);

    /**
     * Returns a new `TweetSet` that is the union of `TweetSet`s `this` and `that`.
     *
     * Question: Should we implment this method here, or should it remain abstract
     * and be implemented in the subclasses?
     */
    public abstract TweetSet union(TweetSet that);

    public abstract Set<Tweet> asSet();

    /**
     * Returns the tweet from this set which has the greatest retweet count.
     *
     * Calling `mostRetweeted` on an empty set should throw an exception of
     * type `java.util.NoSuchElementException`.
     */
    public abstract Tweet mostRetweeted();

    /**
     * Returns a list containing all tweets of this set, sorted by retweet count
     * in descending order. In other words, the head of the resulting list should
     * have the highest retweet count.
     */
    public abstract TweetList descendingByRetweet();

    /**
     * Returns a new `TweetSet` which contains all elements of this set, and the
     * the new element `tweet` in case it does not already exist in this set.
     *
     * If `this.contains(tweet)`, the current set is returned.
     */
    public abstract TweetSet incl(Tweet tweet);

    /**
     * Returns a new `TweetSet` which excludes `tweet`.
     */
    public abstract TweetSet remove(Tweet tweet);

    /**
     * Tests if `tweet` exists in this `TweetSet`.
     */
    public abstract Boolean contains(Tweet tweet);

    /**
     * This method takes a function and applies it to every element in the set.
     * @param f
     */
    public abstract void foreach(Consumer<Tweet> f);

    public static TweetSet asTweetSet(List<Tweet> tweets) {
        return myUnion(tail(tweets), new EmptyTweetSet().incl(tweets.get(0)));
    }

    private static List<Tweet> tail(List<Tweet> tweets) {
        return tweets.size() == 1 ? new ArrayList<>()
                : tweets.subList(1, tweets.size());
    }

    private static TweetSet myUnion(List<Tweet> tweets, TweetSet acc) {
        return tweets.isEmpty() ? acc
                : myUnion(tail(tweets), acc.incl(tweets.get(0)));
    }

}

