package org.coursera.progfun.objsets;

import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
class EmptyTweetSet extends TweetSet {

    @Override
    protected TweetSet filterAcc(Function<Tweet, Boolean> function, TweetSet tweetSet) {
        return tweetSet;
    }

    @Override
    public TweetSet union(TweetSet that) {
        return that;
    }

    @Override
    public Set<Tweet> asSet() {
        return Collections.emptySet();
    }

    @Override
    public Tweet mostRetweeted() {
        throw new NoSuchElementException();
    }

    @Override
    public TweetList descendingByRetweet() {
        return new EmptyTweetList();
    }

    @Override
    public TweetSet incl(Tweet tweet) {
        return new NonEmptyTweetSet(tweet, new EmptyTweetSet(), new EmptyTweetSet());
    }

    @Override
    public TweetSet remove(Tweet tweet) {
        return this;
    }

    @Override
    public Boolean contains(Tweet tweet) {
        return false;
    }

    @Override
    public void foreach(Consumer<Tweet> f) {}
}
