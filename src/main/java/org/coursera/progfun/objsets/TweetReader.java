package org.coursera.progfun.objsets;

import javax.json.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class TweetReader {

    private final Map<String, List<Tweet>> tweetMap;
    private final List<Tweet> tweetList;

    public final List<Tweet> gizmodoTweets;
    public final List<Tweet> techCrunchTweets;
    public final List<Tweet> engadgetTweets;
    public final List<Tweet> amazondealsTweets;
    public final List<Tweet> cnetTweets;
    public final List<Tweet> gadgetlabTweets;
    public final List<Tweet> mashableTweets;
    private final TweetSet tweetSet;

    public TweetReader(InputStream is) throws IOException {
        tweetList = parse(is);
        tweetMap = tweetList.stream().collect(Collectors.groupingBy(t -> t.user));
        tweetSet = TweetSet.asTweetSet(tweetList);

        gizmodoTweets = tweetMap.get("gizmodo");
        techCrunchTweets = tweetMap.get("TechCrunch");
        engadgetTweets = tweetMap.get("engadget");
        amazondealsTweets = tweetMap.get("amazondeals");
        cnetTweets = tweetMap.get("CNET");
        gadgetlabTweets = tweetMap.get("gadgetlab");
        mashableTweets = tweetMap.get("mashable");
    }

    List<Tweet> parse(InputStream is) throws IOException {
        try (JsonReader rdr = Json.createReader(is)) {
            return rdr.readArray().getValuesAs(JsonObject.class).stream().map(
                    (item) -> (new Tweet(item.getString("user"), item.getString("text"), item.getInt("retweets")))
            ).collect(toList());
        }
    }

    public TweetSet allTweets() {
        return tweetSet;
    }

    public Map<String, List<Tweet>> getTweetMap() {
        return Collections.unmodifiableMap(tweetMap);
    }

}
