package org.coursera.progfun.objsets;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class EmptyTweetList implements TweetList {
    @Override
    public Tweet head() {
        throw new java.util.NoSuchElementException("head of EmptyList");
    }

    @Override
    public TweetList tail() {
        throw new java.util.NoSuchElementException("tail of EmptyList");
    }

    @Override
    public Boolean isEmpty() {
        return true;
    }
}
