package org.coursera.progfun.objsets;

import java.util.function.Consumer;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
interface TweetList{

    Tweet head();
    TweetList tail();
    Boolean isEmpty();

    default void foreach(Consumer<Tweet> f) {
        if (!isEmpty()) {
            f.accept(head());
            tail().foreach(f);
        }
    }

    default int size() {
        return isEmpty() ? 0
                : 1 + tail().size();
    }
}
