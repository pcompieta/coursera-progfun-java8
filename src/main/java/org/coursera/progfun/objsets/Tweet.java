package org.coursera.progfun.objsets;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class Tweet implements Comparable{

    public final String user, text;
    public final int retweet;

    public Tweet(String user, String text, int retweet) {
        this.user = user;
        this.text = text;
        this.retweet = retweet;
    }

    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public int getRetweet() {
        return retweet;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "user='" + user + '\'' +
                ", text='" + text + '\'' +
                ", retweet=" + retweet +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return text.compareTo(((Tweet) o).text);
    }

    public boolean before(Tweet other){
        return compareTo(other) < 0;
    }
}
