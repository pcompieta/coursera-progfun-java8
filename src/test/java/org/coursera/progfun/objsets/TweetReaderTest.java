package org.coursera.progfun.objsets;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class TweetReaderTest {

    @Test
    public void testParse() throws Exception {
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("org/coursera/progfun/objsets/TweetData.json")) {
            new TweetReader(is).getTweetMap().entrySet().forEach(System.out::println);
        }
    }
}