package org.coursera.progfun.objsets;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

/**
 * Created by Paolo Compieta on 24/05/2015.
 */
public class TweetSetTest {

    private TweetSet set1 = new EmptyTweetSet();
    private Tweet a = new Tweet("a", "a body", 20);
    private TweetSet set2 = set1.incl(a);
    private Tweet b = new Tweet("b", "b body", 21);
    private TweetSet set3 = set2.incl(b);
    private Tweet c = new Tweet("c", "c body", 7);
    private Tweet d = new Tweet("d", "d body", 9);
    private TweetSet set4c = set3.incl(c);
    private TweetSet set4d = set3.incl(d);
    private TweetSet set5 = set4c.incl(d);

    @Test
    public void testSize() throws Exception {
        assertEquals(0, set1.size());
        assertEquals(1, set2.size());
        assertEquals(2, set3.size());
        assertEquals(3, set4c.size());
        assertEquals(3, set4d.size());
        assertEquals(4, set5.size());
    }

    @Test
    public void emptyDoesNotContain() throws Exception {
        assertTrue(!set1.contains(c));
        assertTrue(!set1.contains(d));
    }

    @Test
    public void nonEmptyContains() throws Exception {
        assertTrue(set5.contains(a));
        assertTrue(set5.contains(b));
        assertTrue(set5.contains(c));
        assertTrue(set5.contains(d));
    }

    @Test
    public void removeElemFromEmpty() throws Exception {
        TweetSet stillEmptySet = new EmptyTweetSet().remove(a);
        assertEquals(0, stillEmptySet.size());
    }

    @Test
    public void removeElemFromNonEmpty() throws Exception {
        TweetSet set5minusA = set5.remove(a);
        assertEquals(3, set5minusA.size());
        assertTrue(!set5minusA.contains(a));
        assertTrue(set5minusA.contains(d));
        assertTrue(set5minusA.contains(c));
        assertTrue(set5minusA.contains(b));
    }

    @Test
    public void emptyShouldContainAfterInclude() throws Exception {
        TweetSet onlyA = new EmptyTweetSet().incl(a);
        assertEquals(1, onlyA.size());
        assertTrue(onlyA.contains(a));
    }

    @Test
    public void nonEmptyIncludesProperly() throws Exception {
        TweetSet set2PlusD = set2.incl(d);
        assertEquals(set2PlusD.size(), set2.size() + 1);
        assertTrue(set2PlusD.contains(d));
        assertTrue(!set2.contains(d));
    }

    @Test
    public void inclAlreadyPresentShouldDoNothing() throws Exception {
        TweetSet set2PlusD = set2.incl(a);
        assertEquals(set2PlusD.size(), set2.size());
        assertTrue(set2PlusD.contains(a));
    }

    @Test
    public void foreachCountretweets() throws Exception {
        final int totRetweet = set3.asSet().stream().mapToInt(Tweet::getRetweet).sum();
        assertEquals(40, totRetweet);
    }

    @Test
    public void filterOnEmptySet() throws Exception {
        assertEquals(0, set1.filter(t -> t.getUser().equals("a")).size());
    }

    @Test
    public void filterByUser() throws Exception {
        assertEquals(1, set5.filter(t -> t.getUser().equals("a")).size());
    }

    @Test
    public void filterByRetweets() throws Exception {
        assertEquals(2, set5.filter(t -> t.getRetweet() == 20).size());
    }

    @Test
    public void filterMoreThan10() throws Exception {
        TweetSet moreThan10 = set5.filter(t -> t.getRetweet() > 10);
        assertEquals(2, moreThan10.size());
        assertTrue(moreThan10.contains(a));
        assertTrue(moreThan10.contains(b));
        assertTrue(!moreThan10.contains(c));
        assertTrue(!moreThan10.contains(d));
    }

    @Test
    public void unionSet4() throws Exception {
        final TweetSet union = set4c.union(set4d);
        assertEquals(4, union.size());
        assertTrue(union.contains(a));
        assertTrue(union.contains(b));
        assertTrue(union.contains(c));
        assertTrue(union.contains(d));
    }

    @Test
    public void unionWithEmpty() throws Exception {
        assertEquals(4, set5.union(set1).size());
        assertEquals(4, set1.union(set5).size());
    }

    @Test
    public void mostRetweeted() throws Exception {
        assertEquals(b, set5.mostRetweeted());
    }

    @Test
    public void descendingByRetweet() throws Exception {
        TweetList descendingByRetweet = set5.descendingByRetweet();
        assertEquals(4, descendingByRetweet.size());
        assertEquals(b, descendingByRetweet.head());
        assertEquals(a, descendingByRetweet.tail().head());
        assertEquals(d, descendingByRetweet.tail().tail().head());
        assertEquals(c, descendingByRetweet.tail().tail().tail().head());
    }

    private IntFunction<Tweet> intToTweet = this::buildTweet;

    private TweetSet buildTweetSet(int n, IntFunction<Tweet> f){
        return asTweetSet(IntStream.rangeClosed(1, n).mapToObj(f).collect(toList()));
    }

    private TweetSet asTweetSetRecursive_usingUnion(List<Tweet> tweets, TweetSet acc) {
        return tweets.isEmpty() ? acc
                : asTweetSetRecursive_usingUnion(tail(tweets), acc.union(new NonEmptyTweetSet(tweets.get(0), new EmptyTweetSet(), new EmptyTweetSet())));
    }

    private List<Tweet> tail(List<Tweet> tweets) {
        return tweets.size() == 1 ? new ArrayList<>()
                : tweets.subList(1, tweets.size());
    }

    private TweetSet asTweetSet(List<Tweet> tweets) {
        return asTweetSetRecursive_usingUnion(tail(tweets), new EmptyTweetSet().incl(tweets.get(0)));
    }

    @Test
    public void unionPerformance1to100() throws Exception {
        IntStream.range(1, 100).forEach(i -> buildTweetSet(i, intToTweet));
    }

    @Test
    public void unionPerformance200plus300() throws Exception {
        buildTweetSet(200, intToTweet).union(buildTweetSet(300, value -> buildTweet(-value))); //.foreach(System.out::println);
    }

    private Tweet buildTweet(int value) {
        return new Tweet("" + value, value + " body", value);
    }
}