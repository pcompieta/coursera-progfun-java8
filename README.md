# README #

### What is this repository for? ###

This project wants to show a few differences between Java8 and Scala when working on Collections and Recursion.

It is a Java 8 implementation of **Data and Abstraction - Object-Oriented Sets** assignments of course [Functional Programming Principles in Scala](https://class.coursera.org/progfun-005) on [Coursera.org](http://coursera.org).
For those registered in coursera, complete link is: https://class.coursera.org/progfun-005/assignment/view?assignment_id=9

### How do I get set up? ###

* Install JDK 8.
* Install any version of Maven compatible with JDK 8.
* Open the project in you favourite IDE (IntelliJ just "understands" the pom.xml in the root)

### How to run tests ###

Just **mvn test** in command line via Maven, or "Run Tests" in your IDE.

### Contribution guidelines ###

If you want to add implementation of more assesments/exercises from the same course, please start from a completed assignment with at least 9.0/10.0 score.

* Implementation
    * For each Scala class, create a Java8 equivalent.
    * No particular formatting required, just be concise to show the power and cleannes of the language.
    * Avoid all 3rd party libraries: only JDK and JEE (javax.*) libraries admitted here.
* Writing tests
    * Be sure to (re-)implement lots of tests you already, to effectively validate the Java8 implementation too.
    * Validate them with **mvn test** as usual
* Code review: a pull request would be enough for me to have a look and then merge things.